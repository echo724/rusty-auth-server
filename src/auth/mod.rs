use std::env;

use chrono::Utc;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub sub: String,
    pub exp: usize,
}

fn create_token(
    user_email: &String,
    expriation_time: i64,
) -> Result<String, jsonwebtoken::errors::Error> {
    let header = Header::default();
    let key = env::var("JWT_SECRET").expect("JWT_SECRET must be set");
    let key = EncodingKey::from_secret(key.as_ref());
    let claims = Claims {
        sub: user_email.to_string(),
        exp: (chrono::Utc::now() + chrono::Duration::hours(expriation_time)).timestamp() as usize,
    };
    encode(&header, &claims, &key)
}

pub fn create_access_token(user_email: &String) -> Result<String, jsonwebtoken::errors::Error> {
    create_token(user_email, 1)
}

pub fn create_refresh_token(user_email: &String) -> Result<String, jsonwebtoken::errors::Error> {
    create_token(user_email, 24)
}

pub fn validate_token(token: &str) -> Result<Claims, jsonwebtoken::errors::Error> {
    let token = token.replace("Bearer ", "");
    let key = env::var("JWT_SECRET").expect("JWT_SECRET must be set");
    let validation = jsonwebtoken::Validation::default();
    let decoding_key = DecodingKey::from_secret(key.as_ref());
    decode::<Claims>(&token, &decoding_key, &validation).map(|data| data.claims)
}

pub fn check_expiration(claims: &Claims) -> bool {
    let now = Utc::now().timestamp() as usize;
    now <= claims.exp
}

pub fn validate_sub(sub: &String, claims: &Claims) -> bool {
    sub == &claims.sub
}
