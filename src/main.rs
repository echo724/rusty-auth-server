mod account;
mod auth;
mod db;
use actix_web::{get, web, App, HttpServer, Responder};
use db::{get_client, init_db};

#[get("/")]
async fn check_server() -> impl Responder {
    "Server is up and running!"
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    init_db(&get_client().await).await;
    HttpServer::new(|| {
        App::new()
            .service(check_server)
            .service(web::scope("/account").configure(account::urls::init_routes))
    })
    .bind(("0.0.0.0", 9999))?
    .run()
    .await
}
