use mongodb::bson::doc;

use crate::account::model::User;
use crate::db::get_auth_db;

use super::model::UserResponse;

pub trait Repository {
    fn new() -> Self;
    fn get_user_info(&self, user_email: &String) -> Result<UserResponse, mongodb::error::Error>;
    fn create_user(&self, user: User) -> Result<(), mongodb::error::Error>;
    fn delete_user(&self, user_email: &String) -> Result<(), mongodb::error::Error>;
    fn verify_user(
        &self,
        user_email: &String,
        password: &String,
    ) -> Result<bool, mongodb::error::Error>;
}

pub struct UserRepository {}

pub async fn get_user_info(user_email: &String) -> Result<UserResponse, mongodb::error::Error> {
    let user = find_user(user_email).await?;
    Ok(UserResponse {
        email: user.email,
        name: user.name,
    })
}

pub async fn create_user(user: User) -> Result<(), mongodb::error::Error> {
    get_db_users().await.insert_one(user, None).await?;
    Ok(())
}

pub async fn delete_user(user_email: &String) -> Result<(), mongodb::error::Error> {
    get_db_users()
        .await
        .delete_one(doc! {"email": user_email}, None)
        .await?;
    Ok(())
}

pub async fn verify_user(
    user_email: &String,
    password: &String,
) -> Result<bool, mongodb::error::Error> {
    let user = find_user(user_email).await?;
    if user.match_password(password) {
        Ok(true)
    } else {
        Ok(false)
    }
}

async fn get_db_users() -> mongodb::Collection<User> {
    get_auth_db().await.collection::<User>("users")
}

async fn find_user(user_email: &String) -> Result<User, mongodb::error::Error> {
    let user = get_db_users()
        .await
        .find_one(doc! {"email": user_email}, None)
        .await?
        .unwrap();
    Ok(user)
}
