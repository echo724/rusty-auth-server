use crate::account::views;
use actix_web::web;

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(views::register);
    cfg.service(views::login);
    cfg.service(views::delete_user);
    cfg.service(views::auth_user);
    cfg.service(views::user_info);
}
