use crate::account::model::{LoginRequest, SignupRequest, User};
use crate::account::repository;
use crate::auth::{
    check_expiration, create_access_token, create_refresh_token, validate_sub, validate_token,
};
use actix_web::{delete, get, post, web, HttpRequest, HttpResponse, Responder};
use serde_json::json;

#[get("/user/{email}")]
pub async fn user_info(path: web::Path<(String,)>) -> impl Responder {
    let user_email = path.into_inner().0;
    let result = repository::get_user_info(&user_email).await;
    match result {
        Ok(user) => HttpResponse::Ok().json(user),
        _ => HttpResponse::NotFound().json("User not found"),
    }
}

#[post("/register")]
pub async fn register(request: web::Json<SignupRequest>) -> impl Responder {
    let user = User::new(&request.email, &request.name, &request.password);
    let result = repository::create_user(user).await;
    match result {
        Ok(_) => HttpResponse::Ok().json("User created"),
        _ => HttpResponse::InternalServerError().json("Error occur while creating user"),
    }
}

#[delete("/delete/{email}")]
pub async fn delete_user(path: web::Path<(String,)>) -> impl Responder {
    let user_email = path.into_inner().0;
    let result = repository::delete_user(&user_email).await;
    match result {
        Ok(_) => HttpResponse::Ok().json("User deleted"),
        _ => HttpResponse::InternalServerError().json("Error occur while deleting user"),
    }
}

#[post("/login")]
pub async fn login(request: web::Json<LoginRequest>) -> impl Responder {
    let result = repository::verify_user(&request.email, &request.password).await;
    match result {
        Ok(true) => {
            let user = repository::get_user_info(&request.email).await.unwrap();
            let access_token = create_access_token(&user.email).unwrap();
            let refresh_token = create_refresh_token(&user.email).unwrap();
            HttpResponse::Ok().json(json!({
                "access_token": access_token,
                "refresh_token": refresh_token,
            }))
        }
        Ok(false) => HttpResponse::Unauthorized().json("User not verified"),
        _ => HttpResponse::InternalServerError().json("Error occur while verifying user"),
    }
}

#[get("/auth/{email}")]
pub async fn auth_user(path: web::Path<String>, req: HttpRequest) -> impl Responder {
    let user_email = path.into_inner();
    let auth = req
        .headers()
        .get("Authorization")
        .map(|v| v.to_str().unwrap());
    match auth {
        Some(token) => {
            let result = validate_token(token);
            let claims = match result {
                Ok(claims) => claims,
                Err(_) => return HttpResponse::Unauthorized().json("Invalid token"),
            };
            if validate_sub(&user_email, &claims) {
                if check_expiration(&claims) {
                    HttpResponse::Ok().json("User verified")
                } else {
                    HttpResponse::Unauthorized().json("Token expired")
                }
            } else {
                HttpResponse::Unauthorized().json("User not verified")
            }
        }
        None => HttpResponse::Unauthorized().json("Authorization header not found"),
    }
}
