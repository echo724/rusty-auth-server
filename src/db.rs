use mongodb::{
    bson::doc,
    options::{ClientOptions, IndexOptions},
    Client, IndexModel,
};
use std::env;

use crate::account::model::User;

pub async fn get_auth_db() -> mongodb::Database {
    let client = get_client().await;
    client.database("auth")
}

pub async fn get_client() -> Client {
    let url = env::var("MONGO_URL").expect("MONGO_URL must be set");
    let client_options = ClientOptions::parse(url).await.unwrap();
    Client::with_options(client_options).unwrap()
}

pub async fn init_db(client: &Client) {
    let options = IndexOptions::builder().unique(true).build();
    let model = IndexModel::builder()
        .keys(doc! {"email": 1})
        .options(options)
        .build();

    client
        .database("auth")
        .collection::<User>("users")
        .create_index(model, None)
        .await
        .expect("Error occur while initializing database");
}
